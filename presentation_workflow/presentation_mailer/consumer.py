import json
import pika
import django
import os
import sys
import time
from django.core.mail import send_mail
from pika.exceptions import AMQPConnectionError



sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


while True:
    try:
        # parameters = pika.ConnectionParameters(host='rabbitmq')
        connection = pika.BlockingConnection(pika.ConnectionParameters("rabbitmq"))
        channel = connection.channel()
        channel.queue_declare(queue='presentation_approvals')
        channel.queue_declare(queue="presentation_rejections")

        def process_approval(ch, method, properties, body):
            data = json.loads(body)
            name = data["presenter_name"]
            title = data["title"]
            email = data["presenter_email"]
            send_mail(
                "Your presentation has been accepted",
                f"{name}, we're happy to tell you that your presentation {title} has been accepted",
                "admin@conferece.go",
                [email],
                fail_silently=False,
            )
        def process_rejection(ch, method, properties, body):
            data = json.loads(body)
            name = data["presenter_name"]
            title = data["title"]
            email = data["presenter_email"]
            send_mail(
                "Your presentation has been rejected",
                f"We're sorry, {name}, but your presentation {title} has been rejected",
                "admin@conferece.go",
                [email],
                fail_silently=False,
            )


        channel.basic_consume(
            queue='presentation_approvals',
            on_message_callback=process_approval,
            auto_ack=True,
        )
        channel.basic_consume(
            queue='presentation_rejections',
            on_message_callback=process_rejection,
            auto_ack=True,
        )
        channel.start_consuming()
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
